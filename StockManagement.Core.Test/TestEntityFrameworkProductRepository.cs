using Microsoft.EntityFrameworkCore;
using Moq;
using StockManagement.Core.Models;
using StockManagement.Core.Repositories;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace StockManagement.Core.Test
{
   public class TestEntityFrameworkProductRepository
   {
      [Fact]
      public async void GetAll()
      {
         var mockData = new List<Product>()
         {
            new Product { AgeRestriction = 5, Company = "Magna proident", Description = "Cupidatat do sunt anim deserunt incididunt reprehenderit.", Id = -4, Name = "Ea exercitation dolor", Price = 14.56m },
            new Product { AgeRestriction = 3, Company = "Ex elit", Description = "Nulla officia ut Lorem minim. Cillum commodo ex duis Lorem consectetur ex.", Id = -3, Name = "Ipsum proident consequat", Price = 5.29m },
            new Product { AgeRestriction = 0, Company = "Duis eiusmod", Description = "Est id cillum dolore nulla culpa incididunt mollit. Anim in magna eu labore labore id est.", Id = -2, Name = "Officia qui eu", Price = 1.99m },
            new Product { AgeRestriction = 9, Company = "Excepteur qui", Description = "Lorem esse exercitation. Non anim duis ea adipisicing ipsum commodo.", Id = -1, Name = "Cupidatat consectetur", Price = 345.89m }
         };
         var dbSet = new Mock<DbSet<Product>>();

         dbSet.As<IQueryable<Product>>().Setup(m => m.Provider).Returns(mockData.AsQueryable().Provider);
         dbSet.As<IQueryable<Product>>().Setup(m => m.Expression).Returns(mockData.AsQueryable().Expression);
         dbSet.As<IQueryable<Product>>().Setup(m => m.ElementType).Returns(mockData.AsQueryable().ElementType);
         dbSet.As<IAsyncEnumerable<Product>>().Setup(m => m.GetEnumerator()).Returns(mockData.ToAsyncEnumerable().GetEnumerator());
         dbSet.Setup(d => d.Add(It.IsAny<Product>())).Callback<Product>((s) => mockData.Add(s));

         var stockContext = new Mock<StockContext>();
         stockContext.Setup(m => m.Set<Product>()).Returns(dbSet.Object);

         var context = stockContext.Object;

         var productRepository = new EntityFrameworkProductRepository(context);

         var actualSeedData = await productRepository.Get();

         Assert.Equal(4, actualSeedData.ToList().Count);
      }

      [Fact]
      public async void GetSingle()
      {
         var mockData = new List<Product>()
         {
            new Product { AgeRestriction = 5, Company = "Magna proident", Description = "Cupidatat do sunt anim deserunt incididunt reprehenderit.", Id = -4, Name = "Ea exercitation dolor", Price = 14.56m },
            new Product { AgeRestriction = 3, Company = "Ex elit", Description = "Nulla officia ut Lorem minim. Cillum commodo ex duis Lorem consectetur ex.", Id = -3, Name = "Ipsum proident consequat", Price = 5.29m },
            new Product { AgeRestriction = 0, Company = "Duis eiusmod", Description = "Est id cillum dolore nulla culpa incididunt mollit. Anim in magna eu labore labore id est.", Id = -2, Name = "Officia qui eu", Price = 1.99m },
            new Product { AgeRestriction = 9, Company = "Excepteur qui", Description = "Lorem esse exercitation. Non anim duis ea adipisicing ipsum commodo.", Id = -1, Name = "Cupidatat consectetur", Price = 345.89m }
         };
         var dbSet = new Mock<DbSet<Product>>();
         dbSet.Setup(m => m.FindAsync(It.IsAny<object[]>())).ReturnsAsync(mockData[1]);

         var stockContext = new Mock<StockContext>();
         stockContext.Setup(m => m.Set<Product>()).Returns(dbSet.Object);

         var context = stockContext.Object;

         var productRepository = new EntityFrameworkProductRepository(context);

         var actualSeedData = await productRepository.Get(-3);

         Assert.Equal(3, actualSeedData.AgeRestriction);
         Assert.Equal("Ex elit", actualSeedData.Company);
         Assert.Equal("Nulla officia ut Lorem minim. Cillum commodo ex duis Lorem consectetur ex.", actualSeedData.Description);
         Assert.Equal(-3, actualSeedData.Id);
         Assert.Equal("Ipsum proident consequat", actualSeedData.Name);
         Assert.Equal(5.29m, actualSeedData.Price);
      }

      [Fact]
      public async void InsertSingle()
      {
         var context = InMemoryStockContext.InMemoryStockContextBuilder();

         var productRepository = new EntityFrameworkProductRepository(context);

         var expectedData = new Product()
         {
            AgeRestriction = 15,
            Company = "Velit cupidatat",
            Description = "Ea cillum aute dolore ut id ex eiusmod sint.Mollit laboris duis duis.",
            Name = "Laboris cupidatat consequat",
            Price = 15.79m
         };

         var actualData = expectedData;
         await productRepository.Insert(actualData);

         Assert.Same(expectedData, actualData);
         Assert.Equal(1, actualData.Id);
      }
   }
}
