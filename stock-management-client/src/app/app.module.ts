import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { environment } from 'src/environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BackendConfig } from './backend-config';
import { DeleteConfirmModule } from './delete-confirm/delete-confirm.module';
import { EditModalModule } from './edit-modal/edit-modal.module';
import { ProductListModule } from './product-list/product-list.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    AppRoutingModule,
    CommonModule,
    BrowserModule,
    DeleteConfirmModule,
    EditModalModule,
    HttpClientModule,
    NgbModule,
    ProductListModule,
  ],
  providers: [
    {
      provide: BackendConfig,
      useValue: { apiUrl: environment.apiUrl },
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
