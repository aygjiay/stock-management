import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductFormModule } from '../product-form/product-form.module';

import { EditModalComponent } from './edit-modal.component';

@NgModule({
  declarations: [EditModalComponent],
  exports: [EditModalComponent],
  imports: [CommonModule, NgbModule, ProductFormModule],
  providers: [],
})
export class EditModalModule {}
