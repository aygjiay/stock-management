import {
  AfterViewInit,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { ProductFormComponent } from '../product-form/product-form.component';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
})
export class EditModalComponent implements AfterViewInit, OnDestroy, OnInit {
  @Input() productId: number;

  disableSave: '' | undefined;

  mainButtonText: 'Save' | 'Close';

  mainButtonAction: () => void;

  @ViewChild(ProductFormComponent)
  private productFormComponent: ProductFormComponent;

  constructor(public modal: NgbActiveModal) {}

  private subscription: Subscription;

  ngOnInit(): void {
    this.mainButtonText = 'Close';
    this.mainButtonAction = this.modal.close;
  }

  ngAfterViewInit(): void {
    const productForm = this.productFormComponent.productForm;

    this.subscription = productForm.valueChanges.subscribe((status) => {
      this.disableSave =
        productForm.dirty || productForm.touched ? undefined : '';

      if (productForm.dirty || productForm.touched) {
        this.mainButtonText = 'Save';
        this.mainButtonAction = this.save;
      } else {
        this.mainButtonText = 'Close';
        this.mainButtonAction = this.modal.close;
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  save() {
    this.productFormComponent.submit();
  }
}
