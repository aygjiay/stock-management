export interface Product {
    ageRestriction: number;

    company: string;

    description: string;

    id: number;

    name: string;

    price: number;
}