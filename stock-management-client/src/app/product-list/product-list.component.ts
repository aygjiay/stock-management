import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import {
  DeleteConfirmComponent,
  DELETE_MODAL_CONFIRM_RESULT,
} from '../delete-confirm/delete-confirm.component';
import { EditModalComponent } from '../edit-modal/edit-modal.component';
import { Product } from '../product';

import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
})
export class ProductListComponent implements OnInit, OnDestroy {
  products: Product[];

  showSpinner: boolean;

  constructor(
    private productService: ProductService,
    private modalService: NgbModal
  ) {}

  private subscription: Subscription;

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.showSpinner = false;

    this.products = [];

    this.subscription = this.productService
      .getProducts()
      .subscribe((productsState) => {
        switch (productsState.status) {
          case 'IDLE':
            this.showSpinner = false;
            break;
          case 'LOADING':
            this.showSpinner = true;
            break;
          case 'ERROR':
            this.showSpinner = false;
            this.products = [];
            break;
          case 'OK':
            this.products = productsState.products;
            break;
        }
      });
  }

  update(productId?: number): void {
    const modalRef = this.modalService.open(EditModalComponent);

    if (productId) {
      modalRef.componentInstance.productId = productId;
    }

    modalRef.result
      .then(() => this.productService.loadProducts())
      .catch(() => this.productService.loadProducts());
  }

  delete(product: Product) {
    const modalRef = this.modalService.open(DeleteConfirmComponent);

    modalRef.componentInstance.modalTitle = 'Product deletion';
    modalRef.componentInstance.itemDescription = `"${product.name}"`;
    modalRef.componentInstance.itemType = ' product';

    modalRef.result
      .then((result) => {
        if (result === DELETE_MODAL_CONFIRM_RESULT) {
          this.productService
            .deleteProduct(product.id)
            .add(() => this.productService.loadProducts());
        }
      })
      .catch(() => {});
  }
}
