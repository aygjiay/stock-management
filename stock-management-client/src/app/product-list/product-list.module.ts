import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ProductListComponent } from './product-list.component';

@NgModule({
    declarations: [
        ProductListComponent
    ],
    exports: [ProductListComponent],
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        ReactiveFormsModule
    ],
    providers: []
})
export class ProductListModule { }
