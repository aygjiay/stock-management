import {
  HttpClient,
  HttpErrorResponse,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import produce from 'immer';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { BackendConfig } from './backend-config';
import { Product } from './product';

export type ProductServiceStatus = 'LOADING' | 'IDLE' | 'ERROR' | 'OK';

interface ProductState {
  currentProduct?: Product;
  currentProductErrors?: { [key: string]: string[] };
  products: Product[];
  serviceStatus: ProductServiceStatus;
  generalError: string;
}

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(
    private httpClient: HttpClient,
    private backendConfig: BackendConfig
  ) {
    this.productState = {
      currentProduct: undefined,
      currentProductErrors: {},
      generalError: '',
      products: [],
      serviceStatus: 'IDLE',
    };

    this.productState$ = new BehaviorSubject(this.productState);
  }

  private productState: ProductState;

  private productState$: BehaviorSubject<ProductState>;

  loadProducts() {
    this.productState = produce(this.productState, (draft) => {
      draft.generalError = '';
      draft.products = [];
    });

    this.productState = ProductService.emitNewState(
      this.productState,
      'LOADING',
      this.productState$
    );

    this.httpClient
      .get<Product[]>(this.backendConfig.apiUrl, { observe: 'response' })
      .pipe(first())
      .subscribe(
        (response) => {
          this.productState = produce(this.productState, (draft) => {
            draft.products = response.body;
          });

          this.productState = ProductService.emitNewState(
            this.productState,
            'OK',
            this.productState$
          );
        },
        () => {
          this.productState = produce(this.productState, (draft) => {
            draft.generalError = 'Unexpected server error';
            draft.products = [];
          });

          this.productState = ProductService.emitNewState(
            this.productState,
            'ERROR',
            this.productState$
          );
        }
      )
      .add(
        () =>
          (this.productState = ProductService.emitNewState(
            this.productState,
            'IDLE',
            this.productState$
          ))
      );
  }

  getProducts() {
    this.loadProducts();

    return this.productState$.pipe(
      map((serviceStatus) => ({
        products: serviceStatus.products,
        error: serviceStatus.generalError,
        status: serviceStatus.serviceStatus,
      }))
    );
  }

  getProduct(productId: number = undefined) {
    this.productState = produce(this.productState, (draft) => {
      draft.currentProductErrors = {};
      draft.generalError = '';
      draft.currentProduct = undefined;
    });

    this.productState = ProductService.emitNewState(
      this.productState,
      'LOADING',
      this.productState$
    );

    if (productId) {
      this.httpClient
        .get<Product>(`${this.backendConfig.apiUrl}/${productId}`, {
          observe: 'response',
        })
        .pipe(first())
        .subscribe(
          (response) => {
            this.productState = produce(this.productState, (draft) => {
              draft.currentProduct = response.body;
              draft.serviceStatus = 'OK';
            });

            this.productState = ProductService.emitNewState(
              this.productState,
              'OK',
              this.productState$
            );
          },
          () => {
            this.productState = produce(this.productState, (draft) => {
              draft.currentProduct = undefined;
              draft.generalError = 'Unexpected server error';
            });

            this.productState = ProductService.emitNewState(
              this.productState,
              'ERROR',
              this.productState$
            );
          }
        )
        .add(
          () =>
            (this.productState = ProductService.emitNewState(
              this.productState,
              'IDLE',
              this.productState$
            ))
        );
    } else {
      this.productState = ProductService.emitNewState(
        this.productState,
        'IDLE',
        this.productState$
      );
    }

    return this.productState$.pipe(
      map((serviceStatus) => ({
        error: serviceStatus.generalError,
        errors: serviceStatus.currentProductErrors,
        product: serviceStatus.currentProduct,
        status: serviceStatus.serviceStatus,
      }))
    );
  }

  saveProduct(product: Product) {
    this.productState = produce(this.productState, (draft) => {
      draft.currentProductErrors = {};
      draft.generalError = '';
      draft.currentProduct = undefined;
    });

    this.productState = ProductService.emitNewState(
      this.productState,
      'LOADING',
      this.productState$
    );

    let httpOperation: Observable<HttpResponse<Product>>;

    if (product.id) {
      httpOperation = this.httpClient.put<Product>(
        `${this.backendConfig.apiUrl}/${product.id}`,
        product,
        { observe: 'response' }
      );
    } else {
      httpOperation = this.httpClient.post<Product>(
        this.backendConfig.apiUrl,
        { ...product, id: undefined },
        {
          observe: 'response',
        }
      );
    }

    httpOperation
      .pipe(first())
      .subscribe(
        (response) => {
          this.productState = produce(this.productState, (draft) => {
            draft.currentProduct = response.body;
            draft.generalError = 'Unexpected server error';
          });

          this.productState = ProductService.emitNewState(
            this.productState,
            'OK',
            this.productState$
          );
        },
        (errorResponse) => {
          this.productState = produce(this.productState, (draft) => {
            draft.currentProduct = product;
            draft.generalError = 'Unexpected server error';

            if (
              errorResponse instanceof HttpErrorResponse &&
              errorResponse.status === 400 &&
              errorResponse.error.modelState
            ) {
              draft.currentProductErrors = {
                ageRestriction:
                  errorResponse.error.modelState['value.AgeRestriction'] || [],
                company: errorResponse.error.modelState['value.Company'] || [],
                description:
                  errorResponse.error.modelState['value.Description'] || [],
                id: errorResponse.error.modelState['value.Id'] || [],
                name: errorResponse.error.modelState['value.Name'] || [],
                price: errorResponse.error.modelState['value.Price'] || [],
              };
            }
          });

          this.productState = ProductService.emitNewState(
            this.productState,
            'ERROR',
            this.productState$
          );
        }
      )
      .add(
        () =>
          (this.productState = ProductService.emitNewState(
            this.productState,
            'IDLE',
            this.productState$
          ))
      );
  }

  deleteProduct(productId: number) {
    this.productState = produce(this.productState, (draft) => {
      draft.currentProductErrors = {};
      draft.generalError = '';
      draft.currentProduct = undefined;
    });

    this.productState = ProductService.emitNewState(
      this.productState,
      'LOADING',
      this.productState$
    );

    return this.httpClient
      .delete<Product>(`${this.backendConfig.apiUrl}/${productId}`, {
        observe: 'response',
      })
      .pipe(first())
      .subscribe(
        () => {
          this.productState = produce(this.productState, (draft) => {
            draft.generalError = '';
          });

          this.productState = ProductService.emitNewState(
            this.productState,
            'OK',
            this.productState$
          );
        },
        () => {
          this.productState = produce(this.productState, (draft) => {
            draft.generalError = 'Unexpected server error';
          });

          this.productState = ProductService.emitNewState(
            this.productState,
            'ERROR',
            this.productState$
          );
        }
      )
      .add(
        () =>
          (this.productState = ProductService.emitNewState(
            this.productState,
            'IDLE',
            this.productState$
          ))
      );
  }

  private static emitNewState(
    currentState: ProductState,
    status: ProductServiceStatus,
    stateSubject: Subject<ProductState>
  ): ProductState {
    const nextState = produce(currentState, (draft) => {
      draft.serviceStatus = status;
    });

    stateSubject.next(nextState);

    return nextState;
  }
}
