import { NgClass } from '@angular/common';
import {
  AfterViewInit,
  Component,
  ContentChild,
  Input,
  OnDestroy,
} from '@angular/core';
import { FormControlName } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-bootstrap-form-control',
  templateUrl: './bootstrap-form-control.component.html',
})
export class BootstrapFormControlComponent implements OnDestroy, AfterViewInit {
  @Input()
  label: string;

  @ContentChild(FormControlName)
  formControlName: FormControlName;

  @ContentChild(NgClass)
  private ngClass: NgClass;

  errorMessages: string[];

  constructor() {}

  private statusSubscription: Subscription;

  ngAfterViewInit(): void {
    this.ngClass.ngClass = {
      ['form-control']: true,
      ['is-invalid']: false,
    };
    this.ngClass.ngDoCheck();

    this.statusSubscription = this.formControlName.statusChanges.subscribe(
      () => {
        const showInvalid =
          this.formControlName.invalid &&
          (this.formControlName.touched || this.formControlName.dirty);

        this.ngClass.ngClass = {
          ['form-control']: true,
          ['is-invalid']: showInvalid,
        };
        this.ngClass.ngDoCheck();

        this.errorMessages = [];

        if (showInvalid) {
          for (const key in this.formControlName.errors) {
            if (
              Object.prototype.hasOwnProperty.call(
                this.formControlName.errors,
                key
              ) &&
              this.formControlName.errors[key]
            ) {
              this.errorMessages.push(
                BootstrapFormControlComponent.errorToMessage(
                  key,
                  this.formControlName.errors[key],
                  this.label
                )
              );
            }
          }
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.statusSubscription.unsubscribe();
  }

  static errorToMessage(error, value, label): string {
    const messages: { [key: string]: string } = {
      required: 'The {{fieldLabel}} field is required.',
      maxlength:
        'The field {{fieldLabel}} must be a string with a maximum length of {{value}}.',
      min: 'The field {{fieldLabel}} must be greater than {{value}}.',
      max: 'The field {{fieldLabel}} must be less than {{value}}.',
    };

    const valueMapper: { [key: string]: (value: any) => string } = {
      maxlength: (value) => value.requiredLength,
      min: (value) => JSON.stringify(value.min),
      max: (value) => JSON.stringify(value.max),
    };

    const errorMessage = (messages[error] || error).replace(
      /{{fieldLabel}}/,
      label || ''
    );

    if (valueMapper[error]) {
      return errorMessage.replace(/{{value}}/, valueMapper[error](value));
    }

    return errorMessage;
  }
}
