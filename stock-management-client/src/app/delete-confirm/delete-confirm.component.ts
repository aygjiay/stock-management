import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

export const DELETE_MODAL_DISMISS_RESULT = 'Dismiss';
export const DELETE_MODAL_CONFIRM_RESULT = 'Confirm';

@Component({
  selector: 'app-delete-confirm',
  templateUrl: './delete-confirm.component.html',
})
export class DeleteConfirmComponent {
  @Input() modalTitle: string;
  @Input() itemDescription: string;
  @Input() itemType: string;

  dismissResult = DELETE_MODAL_DISMISS_RESULT;

  confirmResult = DELETE_MODAL_CONFIRM_RESULT;

  constructor(public modal: NgbActiveModal) {}
}
