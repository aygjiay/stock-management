import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DeleteConfirmComponent } from './delete-confirm.component';

@NgModule({
  declarations: [DeleteConfirmComponent],
  exports: [DeleteConfirmComponent],
  imports: [CommonModule, NgbModule],
  providers: [],
})
export class DeleteConfirmModule {}
