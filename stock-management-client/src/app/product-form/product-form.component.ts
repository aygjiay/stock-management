import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { Product } from '../product';
import { ProductService, ProductServiceStatus } from '../product.service';

interface ProductState {
  error: string;
  errors: { [key: string]: string[] };
  product: Product;
  status: ProductServiceStatus;
}

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
})
export class ProductFormComponent implements OnInit, OnDestroy {
  @Input() productId?: number;

  productForm: FormGroup;

  showError: boolean;

  showSuccess: boolean;

  subscription: Subscription;

  inputErrors: { [key: string]: string[] };

  constructor(private productService: ProductService) {
    this.productForm = new FormGroup({
      id: new FormControl(undefined),
      name: new FormControl('', [
        Validators.required,
        Validators.maxLength(50),
      ]),
      price: new FormControl(undefined, [
        Validators.required,
        Validators.min(1),
        Validators.max(1000),
      ]),
      company: new FormControl('', [
        Validators.required,
        Validators.maxLength(50),
      ]),
      ageRestriction: new FormControl(undefined, [
        Validators.min(0),
        Validators.max(100),
      ]),
      description: new FormControl('', Validators.maxLength(100)),
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.showError = false;
    this.showSuccess = false;

    this.subscription = this.productService
      .getProduct(this.productId)
      .subscribe((productState) => {
        switch (productState.status) {
          case 'IDLE':
            this.renderIdleSatus(productState);
            break;
          case 'LOADING':
            this.renderLoadingSatus(productState);
            break;
          case 'ERROR':
            this.renderErrorSatus(productState);
            break;
          case 'OK':
            this.renderOkSatus(productState);
            break;
        }

        this.inputErrors = productState.errors;
      });
  }

  renderErrorSatus(productState: ProductState) {
    this.productForm.enable();
    this.showError = true;

    for (const key in productState.errors) {
      if (Object.prototype.hasOwnProperty.call(productState.errors, key)) {
        const validationErrors = productState.errors[key].reduce(
          (allErrors, error) =>
            Object.assign(allErrors || {}, { [error]: true }),
          undefined as {}
        );

        if (validationErrors) {
          this.showError = false;
          this.productForm.controls[key].setErrors(validationErrors);
        }
      }
    }
  }

  renderLoadingSatus(productState: ProductState) {
    this.productForm.disable();
    this.showError = false;
    this.showSuccess = false;
  }

  renderIdleSatus(productState: ProductState) {}

  renderOkSatus(productState: ProductState) {
    this.productForm.enable();
    this.showSuccess = this.productForm.dirty;

    this.productForm.setValue({
      id: productState.product.id,
      name: productState.product.name,
      price: productState.product.price,
      company: productState.product.company,
      ageRestriction: productState.product.ageRestriction,
      description: productState.product.description,
    });
  }

  closeAlert() {
    this.showError = false;
    this.showSuccess = false;
  }

  submit(): void {
    if (this.productForm.valid) {
      this.productService.saveProduct(this.productForm.value);
    } else {
      this.productForm.markAllAsTouched();
      for (const key in this.productForm.controls) {
        if (
          Object.prototype.hasOwnProperty.call(this.productForm.controls, key)
        ) {
          const formControl = this.productForm.controls[key];

          formControl.updateValueAndValidity();
        }
      }
    }
  }
}
