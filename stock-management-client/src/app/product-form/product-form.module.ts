import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ProductFormAlertsComponent } from '../product-form-alerts/product-form-alerts.component';
import { ProductFormComponent } from './product-form.component';
import { BootstrapFormControlComponent } from '../bootstrap-form-control/bootstrap-form-control.component';

@NgModule({
  declarations: [
    BootstrapFormControlComponent,
    ProductFormAlertsComponent,
    ProductFormComponent,
  ],
  exports: [ProductFormComponent],
  imports: [CommonModule, FormsModule, NgbModule, ReactiveFormsModule],
  providers: [],
})
export class ProductFormModule {}
