import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NgbAlert, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ProductFormAlertsComponent } from './product-form-alerts.component';

describe('ProductFormAlertsComponent', () => {
  let component: ProductFormAlertsComponent;
  let fixture: ComponentFixture<ProductFormAlertsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NgbModule],
      declarations: [ProductFormAlertsComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductFormAlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render server error message', () => {
    component.showSuccess = false;
    component.showServerError = true;
    fixture.detectChanges();

    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('ngb-alert[type="danger"]')).toBeTruthy();
  });

  it('should render success message', () => {
    component.showSuccess = true;
    component.showServerError = false;
    fixture.detectChanges();

    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('ngb-alert[type="success"]')).toBeTruthy();
  });

  it('should emit success message close request', () => {
    spyOn(component.onCloseAlert, 'emit');

    component.showSuccess = true;
    component.showServerError = false;
    fixture.detectChanges();

    const ngbAlert = fixture.debugElement.query(By.directive(NgbAlert));
    const compiled = ngbAlert.nativeElement;
    const button = compiled.querySelector('button');
    button.click();

    fixture.detectChanges();

    expect(component.onCloseAlert.emit).toHaveBeenCalledWith('success');
  });

  it('should emit error message close request', () => {
    spyOn(component.onCloseAlert, 'emit');

    component.showSuccess = false;
    component.showServerError = true;
    fixture.detectChanges();

    const ngbAlert = fixture.debugElement.query(By.directive(NgbAlert));
    const compiled = ngbAlert.nativeElement;
    const button = compiled.querySelector('button');
    button.click();

    fixture.detectChanges();

    expect(component.onCloseAlert.emit).toHaveBeenCalledWith('error');
  });
});
