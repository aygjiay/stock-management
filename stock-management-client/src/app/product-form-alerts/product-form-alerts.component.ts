import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-product-form-alerts',
  templateUrl: './product-form-alerts.component.html'
})
export class ProductFormAlertsComponent {
  @Input() showServerError: boolean;

  @Input() showSuccess: boolean;

  @Output() onCloseAlert = new EventEmitter<'error' | 'success'>();

  closeAlert(source: 'error' | 'success') {
    this.onCloseAlert.emit(source);
  }
}
