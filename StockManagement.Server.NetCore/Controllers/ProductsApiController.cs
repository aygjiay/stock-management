﻿using Microsoft.AspNetCore.Mvc;
using StockManagement.Core.Models;
using StockManagement.Core.Repositories;
using System.Threading.Tasks;

namespace StockManagement.Server.NetCore.Controllers
{
   [Route("api/v1/products")]
   [ApiController]
   public class ProductsApiController : ControllerBase
   {
      private readonly IProductRepository productRepository;

      public ProductsApiController(IProductRepository productRepository)
      {
         this.productRepository = productRepository;
      }

      [HttpGet]
      public async Task<ActionResult> Get()
      {
         var products = await this.productRepository.Get();

         return this.Ok(products);
      }

      [HttpGet("{productId:int}")]
      public async Task<ActionResult> Get(int productId)
      {
         var product = await this.productRepository.Get(productId);

         return this.Ok(product);
      }

      [HttpPost]
      public async Task<ActionResult> Post([FromBody]Product value)
      {
         await this.productRepository.Insert(value);

         return Ok(value);
      }

      [HttpPut("{productId:int}")]
      public async Task<ActionResult> Put(int productId, [FromBody]Product value)
      {
         value.Id = productId;

         await this.productRepository.Update(value);

         return Ok(value);
      }

      [HttpDelete("{productId:int}")]
      public async Task<ActionResult> Delete(int productId)
      {
         await this.productRepository.Delete(productId);

         return this.Ok();
      }

   }
}