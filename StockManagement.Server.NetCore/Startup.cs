using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace StockManagement.Server.NetCore
{
   public class Startup
   {
      public Startup(IConfiguration configuration)
      {
         this.Configuration = configuration;
      }

      public IConfiguration Configuration { get; }

      // This method gets called by the runtime. Use this method to add services to the container.
      public void ConfigureServices(IServiceCollection services)
      {
         services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

         // In production, the Angular files will be served from this directory
         services.AddSpaStaticFiles(configuration =>
      {
         configuration.RootPath = "../StockManagement.Server.NetFramework/Assets/angular";
      });

         services.Add(new ServiceDescriptor(
            typeof(Core.StockContext),
            serviceProvider => Core.InMemoryStockContext.InMemoryStockContextBuilder(),
            ServiceLifetime.Transient));

         services.Add(new ServiceDescriptor(
            typeof(Core.Repositories.IProductRepository),
            typeof(Core.Repositories.EntityFrameworkProductRepository), ServiceLifetime.Transient
         ));
      }

      // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
      public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
      {
         if (env.IsDevelopment())
         {
            app.UseDeveloperExceptionPage();
         }
         else
         {
            app.UseExceptionHandler("/Error");
            app.UseHsts();
         }

         app.UseHttpsRedirection();
         app.UseStaticFiles();
         app.UseSpaStaticFiles();

         app.UseMvc(routes =>
         {
            routes.MapRoute(
                   name: "default",
                   template: "{controller}/{action=Index}/{id?}");
         });

         app.UseSpa(spa =>
         {
            // To learn more about options for serving an Angular SPA from ASP.NET Core,
            // see https://go.microsoft.com/fwlink/?linkid=864501

            spa.Options.SourcePath = "../stock-management-client";

            if (env.IsDevelopment())
            {
               //spa.UseAngularCliServer(npmScript: "start");
               spa.UseProxyToSpaDevelopmentServer("http://localhost:4200");
            }
         });
      }
   }
}
