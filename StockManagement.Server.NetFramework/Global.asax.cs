﻿using LightInject;
using Newtonsoft.Json.Serialization;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace StockManagement.Server.NetFramework
{
  public class MvcApplication : System.Web.HttpApplication
  {
    protected void Application_Start()
    {
      var serviceContainer = new ServiceContainer();
      RegisterDependencies(serviceContainer);

      AreaRegistration.RegisterAllAreas();

      GlobalConfiguration.Configure(WebApiConfig.Register);

      HttpConfiguration config = GlobalConfiguration.Configuration;
      config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
      config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;

      FilterConfig.RegisterGlobalFilters(serviceContainer, GlobalFilters.Filters);

      RouteConfig.RegisterRoutes(RouteTable.Routes);

      BundleConfig.RegisterBundles(BundleTable.Bundles);
    }

    private void RegisterDependencies(IServiceContainer serviceContainer)
    {
      serviceContainer.RegisterControllers(typeof(MvcApplication).Assembly);

      serviceContainer.RegisterApiControllers();
      serviceContainer.EnablePerWebRequestScope();
      serviceContainer.EnableWebApi(GlobalConfiguration.Configuration);

      serviceContainer.Register(serviceFactory => Core.InMemoryStockContext.InMemoryStockContextBuilder(), new PerScopeLifetime());

      serviceContainer.Register<Core.Repositories.IProductRepository, Core.Repositories.EntityFrameworkProductRepository>(new PerScopeLifetime());

      serviceContainer.Register(serviceFactory => log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType), new PerScopeLifetime());

      serviceContainer.Register<IFilter, UnhandledExceptionFilterAttribute>();
      serviceContainer.Register<IFilter, ValidateModelFilterAttribute>();

      serviceContainer.EnableMvc();
    }
  }
}
