﻿using StockManagement.Core.Models;
using StockManagement.Core.Repositories;
using System.Threading.Tasks;
using System.Web.Http;

namespace StockManagement.Server.NetFramework.Controllers
{
  [RoutePrefix("api/v1/products")]
  public class ProductsApiController : ApiController
  {
    readonly IProductRepository productRepository;

    public ProductsApiController(IProductRepository productRepository)
    {
      this.productRepository = productRepository;
    }

    [Route("")]
    public async Task<IHttpActionResult> Get()
    {
      var products = await productRepository.Get();

      return Ok(products);
    }

    [Route("{productId:int}")]
    public async Task<IHttpActionResult> Get(int productId)
    {
      var product = await productRepository.Get(productId);

      return Ok(product);
    }

    [Route("")]
    public async Task<IHttpActionResult> Post([FromBody]Product value)
    {
      await productRepository.Insert(value);

      return Ok(value);
    }

    [Route("{productId:int}")]
    public async Task<IHttpActionResult> Put(int productId, [FromBody]Product value)
    {
      value.Id = productId;

      await productRepository.Update(value);

      return Ok(value);
    }

    [Route("{productId:int}")]
    public async Task<IHttpActionResult> Delete(int productId)
    {
      await productRepository.Delete(productId);

      return Ok();
    }
  }
}