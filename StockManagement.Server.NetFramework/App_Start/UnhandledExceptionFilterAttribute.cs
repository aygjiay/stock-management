﻿using log4net;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace StockManagement.Server.NetFramework
{
  public class UnhandledExceptionFilterAttribute : ExceptionFilterAttribute
  {
    private readonly ILog log;

    public UnhandledExceptionFilterAttribute(ILog log)
    {
      this.log = log;
    }

    public override void OnException(HttpActionExecutedContext actionExecutedContext)
    {
      log.Fatal("Unhandled exception", actionExecutedContext.Exception);

      base.OnException(actionExecutedContext);
    }

    public override Task OnExceptionAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
    {
      log.Fatal("Unhandled exception", actionExecutedContext.Exception);

      return base.OnExceptionAsync(actionExecutedContext, cancellationToken);
    }
  }
}