﻿using System.Web.Optimization;

namespace StockManagement.Server.NetFramework
{
  public class BundleConfig
  {
    public static void RegisterBundles(BundleCollection bundles)
    {
      bundles.Add(new ScriptBundle("~/scripts/angular")
        .Include(
          "~/Assets/angular/inline.*",
          "~/Assets/angular/polyfills.*",
          "~/Assets/angular/scripts.*",
          "~/Assets/angular/vendor.*",
          "~/Assets/angular/runtime.*",
          "~/Assets/angular/main.*"
        )
      );

      bundles.Add(new StyleBundle("~/styles/angular")
        .Include("~/Assets/angular/styles.*")
      );
    }
  }
}
