﻿using LightInject;
using System;
using System.Configuration;
using System.Web.Http;
using System.Web.Http.Filters;

namespace StockManagement.Server.NetFramework
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      var origins = ConfigurationManager.AppSettings["cors.origins"];
      var headers = ConfigurationManager.AppSettings["cors.headers"];
      var methods = ConfigurationManager.AppSettings["cors.methods"];

      var cors = new System.Web.Http.Cors.EnableCorsAttribute(origins, headers, methods);
      config.EnableCors(cors);

      config.MapHttpAttributeRoutes();

      //var filterAttributes = serviceFactory.GetAllInstances<IFilter>();
      var filterAttributes = config.DependencyResolver.GetServices(typeof(IFilter));

      foreach (var filterAttribute in filterAttributes)
      {
        config.Filters.Add(filterAttribute as IFilter);
      }

    }
  }
}
