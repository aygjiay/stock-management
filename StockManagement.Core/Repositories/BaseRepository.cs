﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace StockManagement.Core.Repositories
{
  public class BaseRepository<TRepository, TKey> where TRepository : class
  {
    private readonly DbContext context;

    private readonly DbSet<TRepository> dbSet;

    public BaseRepository(DbContext context)
    {
      this.context = context;
      dbSet = context.Set<TRepository>();
    }

    public virtual async Task<IEnumerable<TRepository>> Get(
        Expression<Func<TRepository, bool>> filter = null,
        Func<IQueryable<TRepository>, IOrderedQueryable<TRepository>> orderBy = null,
        IEnumerable<string> includeProperties = null)
    {
      IQueryable<TRepository> query = dbSet;

      if (filter != null)
      {
        query = query.Where(filter);
      }

      if (includeProperties != null)
      {
        foreach (var includeProperty in includeProperties)
        {
          query = query.Include(includeProperty);
        }
      }

      if (orderBy != null)
      {
        return await orderBy(query).ToListAsync();
      }
      else
      {
        return await query.ToListAsync();
      }
    }

    public virtual async Task<TRepository> Get(TKey id)
    {
      return await dbSet.FindAsync(id);
    }

    public virtual async Task Insert(TRepository entity)
    {
      await dbSet.AddAsync(entity);

      await context.SaveChangesAsync();
    }

    public virtual async Task Delete(TKey id)
    {
      TRepository entityToDelete = await dbSet.FindAsync(id);

      await Delete(entityToDelete);

    }

    public virtual async Task Delete(TRepository entityToDelete)
    {
      if (context.Entry(entityToDelete).State == EntityState.Detached)
      {
        dbSet.Attach(entityToDelete);
      }

      dbSet.Remove(entityToDelete);

      await context.SaveChangesAsync();
    }

    public virtual async Task Update(TRepository entityToUpdate)
    {
      dbSet.Attach(entityToUpdate);
      context.Entry(entityToUpdate).State = EntityState.Modified;

      await context.SaveChangesAsync();
    }
  }
}
