﻿using StockManagement.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace StockManagement.Core.Repositories
{
  public interface IProductRepository
  {
    Task<IEnumerable<Product>> Get(
        Expression<Func<Product, bool>> filter = null,
        Func<IQueryable<Product>, IOrderedQueryable<Product>> orderBy = null,
        IEnumerable<string> includeProperties = null);

    Task<Product> Get(int productId);

    Task Insert(Product product);

    Task Delete(int productId);

    Task Update(Product product);
  }
}
