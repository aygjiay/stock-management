﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StockManagement.Core.Models;

namespace StockManagement.Core.Repositories
{
  public class EntityFrameworkProductRepository : BaseRepository<Product, int>, IProductRepository
  {
    public EntityFrameworkProductRepository(StockContext context) : base(context)
    {
    }
  }
}
