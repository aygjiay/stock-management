﻿using Microsoft.EntityFrameworkCore;
using StockManagement.Core.Models;

namespace StockManagement.Core
{
   public abstract class StockContext : DbContext
   {
      public StockContext() : base()
      {
      }

      public StockContext(DbContextOptions options) : base(options)
      {
      }

      public DbSet<Product> Products { get; set; }
   }
}
