﻿using Microsoft.EntityFrameworkCore;
using StockManagement.Core.Models;

namespace StockManagement.Core
{
   public class InMemoryStockContext : StockContext
   {
      private InMemoryStockContext(DbContextOptions options) : base(options)
      {
      }

      protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => base.OnConfiguring(optionsBuilder);

      protected override void OnModelCreating(ModelBuilder modelBuilder)
      {
         base.OnModelCreating(modelBuilder);

         modelBuilder.Entity<Product>().HasKey(product => product.Id);
         modelBuilder.Entity<Product>().Property(product => product.Id).ValueGeneratedOnAdd();

         modelBuilder.Entity<Product>().Property(product => product.Name).IsRequired().IsUnicode().HasMaxLength(50);

         modelBuilder.Entity<Product>().Property(product => product.Description).IsRequired(false).IsUnicode().HasMaxLength(100);

         modelBuilder.Entity<Product>().Property(product => product.AgeRestriction).IsRequired(false);

         modelBuilder.Entity<Product>().Property(product => product.Company).IsRequired().IsUnicode().HasMaxLength(50);

         modelBuilder.Entity<Product>().Property(product => product.Price).IsRequired();

         modelBuilder.Entity<Product>().HasData(
            new Product { AgeRestriction = 5, Company = "Magna proident", Description = "Cupidatat do sunt anim deserunt incididunt reprehenderit.", Id = -4, Name = "Ea exercitation dolor", Price = 14.56m },
            new Product { AgeRestriction = 3, Company = "Ex elit", Description = "Nulla officia ut Lorem minim. Cillum commodo ex duis Lorem consectetur ex.", Id = -3, Name = "Ipsum proident consequat", Price = 5.29m },
            new Product { AgeRestriction = 0, Company = "Duis eiusmod", Description = "Est id cillum dolore nulla culpa incididunt mollit. Anim in magna eu labore labore id est.", Id = -2, Name = "Officia qui eu", Price = 1.99m },
            new Product { AgeRestriction = 9, Company = "Excepteur qui", Description = "Lorem esse exercitation. Non anim duis ea adipisicing ipsum commodo.", Id = -1, Name = "Cupidatat consectetur", Price = 345.89m }
         );
      }


      public static StockContext InMemoryStockContextBuilder()
      {
         var contextOptionsBuilder = new DbContextOptionsBuilder<InMemoryStockContext>();

         contextOptionsBuilder.UseInMemoryDatabase("/*StockManagementDatabase*/");

         var options = contextOptionsBuilder.Options;

         var context = new InMemoryStockContext(options);

         context.Database.EnsureCreated();

         return context;
      }
   }
}
